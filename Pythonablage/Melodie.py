from Tonleiter import Tonleiter
from random import choice

class Melodie:
    def compose(self, dataModel):
        scale = Tonleiter()
        rhythm = dataModel['Taktart']
        speedOfQuarter = dataModel['SpeedOfQuarter']
        for scene in dataModel['Scenes']:
            for tact in scene['Takte']:
                tones = scale.createTones(scene['Tongeschlecht'], tact['Grundton'])
                timestamp = tact['Start']
                tact['Instrumente']['Piano'] = []
                instrument = tact['Instrumente']['Piano']
                for i in range(rhythm):
                    note = {'Instrument': 'Piano', 'Ton': choice(tones), 'Hoehe': 5, 'Laenge': speedOfQuarter, 'Lautstaerke': 255, 'Start': timestamp}
                    instrument.append(note)
                    timestamp += speedOfQuarter


        return dataModel