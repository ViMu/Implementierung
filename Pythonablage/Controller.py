# Author: Marius Jochheim

from random import choice
from ModelBuilder import ModellBuilder
from Tonleiter import Tonleiter
from Begleitung import Begleitung
from Melodie import Melodie

class Composer:
    def __init__(self, information):
        self.style = information['style']
        self.bpm = information['bpm']
        self.events = information['events']
        self.songlength = information['songlength']
        self.instruments = self.getInstruments(self.style)
        self.sceneChanges = information['sceneChanges']
        self.tact = self.getTact()

    def getInstruments(self, style):
        return ['Guitar', 'Drums', 'Bass', 'Piano']

    def getTact(self):
        possibleTacts = [3, 4]
        return choice(possibleTacts)

    def defineSceneScales(self, dataModel):
        # Get a root for every scene
        scale = Tonleiter()
        scenes = dataModel['Scenes']
        minorList = []
        for scene in scenes:
            minorList.append(scene['Tongeschlecht'])
        sceneRoots = scale.getSceneRoots(amountScenes=len(scenes), listOfMinor=minorList)

        # Append root to every scene
        i = 0
        for sceneRoot in sceneRoots:
            scenes[i]['Tonart'] = sceneRoot[0]
            i += 1

    def defineTactRoots(self, dataModel):
        scale = Tonleiter()
        for scene in dataModel['Scenes']:
            print(scene['Tonart'], scene['Tongeschlecht'])
            rootList = scale.getTactRoots(scene['Tonart'], scene['Tongeschlecht'], len(scene['Takte']))
            i = 0
            for root in rootList:
                scene['Takte'][i]['Grundton'] = root
                i += 1

    def compose(self):
        # Build initial empty Model
        modellBuilder = ModellBuilder(takt = self.tact, laenge = self.songlength,
                                      events = self.events, sceneChanges = self.sceneChanges, stil = self.style, bpm = self.bpm)
        dataModel = modellBuilder.build()

        # Define scales for scenes
        self.defineSceneScales(dataModel)

        # Add Event information
        # Todo Noch nicht gemacht, weil im Moment unnoetig

        # Add root per tact
        self.defineTactRoots(dataModel)

        # Add accompaniment
        accompanie = Begleitung()
        dataModel = accompanie.compose(dataModel)

        # Add Melody
        melody = Melodie()
        dataModel = melody.compose(dataModel)

        # Change volume

        # Smoothen Scene changes

        # Event handling

        return dataModel