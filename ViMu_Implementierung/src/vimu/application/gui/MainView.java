package vimu.application.gui;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 * 
 * @author Tim Schmidt
 *
 */

public class MainView extends Application{
	static Logger rootLogger = Logger.getGlobal();
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		rootLogger.log(Level.INFO, "Start Primary Stage");
		rootLogger.log(Level.INFO, "Started by User: " + System.getProperty("user.name"));
		rootLogger.log(Level.INFO, "Application dir:" + System.getProperty("user.dir"));
		// load the FXML resource
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/ViMu.fxml"));
		// store the root element so that the controllers can use it
		AnchorPane rootElement = (AnchorPane) loader.load();
		// create and style a scene
		Scene scene = new Scene(rootElement);
		//Zentrierung in der Bildschirmmitte TODO: Nochmal r�berschauen
		Rectangle2D primaryScreenBounds = Screen.getPrimary().getVisualBounds();
		double by = primaryScreenBounds.getMinY() + primaryScreenBounds.getHeight()/2;
		double bx = primaryScreenBounds.getMinX() + primaryScreenBounds.getWidth()/2;
		primaryStage.setX(bx - primaryStage.getWidth());
		primaryStage.setY(by - primaryStage.getHeight());
		// create the stage with the given title and show the GUI
		primaryStage.setTitle("ViMu - Automatische Vertonung f�r Skater");
		primaryStage.getIcons().add(new Image("/images/logo.png"));
		primaryStage.setMinHeight(400);
		primaryStage.setMinWidth(650);
		primaryStage.setScene(scene);
		primaryStage.show(); 
		List<String> devMode = getParameters().getUnnamed();
        if (!devMode.isEmpty()&&devMode.get(0).equals("dev")) {
    		vimu.application.gui.DevView dev = new vimu.application.gui.DevView();
    		dev.display();
        }
		//Initialise controller
		loader.getController();
	 	//Set Handler for closing action
		primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
			@Override
            public void handle(WindowEvent t) {
                Platform.exit();
                System.exit(0);
            }
		});
	}
}
