package vimu.application.encoder;

import io.humble.video.Decoder;
import io.humble.video.Demuxer;
import io.humble.video.Encoder;
import io.humble.video.KeyValueBag;
import io.humble.video.MediaAudio;
import io.humble.video.MediaAudioResampler;
import io.humble.video.MediaDescriptor;
import io.humble.video.DemuxerStream;
import io.humble.video.Muxer;
import io.humble.video.MuxerFormat;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import vimu.application.gui.Main;
import io.humble.video.AudioChannel;
import io.humble.video.AudioChannel.Layout;
import io.humble.video.AudioFormat;
import io.humble.video.Codec;
import io.humble.video.Coder;/*
import io.humble.video.MediaDescriptor;
import io.humble.video.MediaDescriptor.Type;
import io.humble.video.AudioChannel.Layout;
import io.humble.video.BitStreamFilter;*/
import io.humble.video.MediaPacket;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.opencv.core.Core;

/**
 * FFMPEG Wrapper to Encode PCM & Merge audio tracks
 * @author Paul Bauriegel
 */
public class VideoEncoder {
	static private Logger logger = Logger.getGlobal();
	

	
	// ffmpeg -y -i Sequence.mp4 -i vimu.wav -filter_complex "[0:a]volume=0.1[a0];[1:a]volume=0.9[a1];[a0][a1]amix=inputs=2[a]" -map 0:v -map "[a]" -c:v copy -c:a libmp3lame -ac 2 -shortest out.mp4
	public static void cmdWrapper(String inputVideo, String inputAudio, String outputFile, double backMusic){
		DecimalFormat format = new DecimalFormat("#.##");
		format.setDecimalFormatSymbols(new DecimalFormatSymbols(Locale.ENGLISH));
		String command = "ffmpeg.exe -y -i \""+inputVideo+"\" -i \""+inputAudio+"\" -filter_complex \"[0:a]volume="+format.format(backMusic/100)+"[a0];[1:a]volume=1[a1];[a0][a1]amix=inputs=2[a]\" -map 0:v -map \"[a]\" -c:v copy -c:a libmp3lame -ac 2 -shortest \""+outputFile+"\"";
		logger.log(Level.INFO, "FFMPEG: " + command);
		try {
			//logger.log(Level.INFO, "Values:" + inputVideo+"\n"+inputAudio+"\n"+outputFile+"\n"+
					//Encoder.class.getResource("/res").toURI());
			//File dir = new File(Encoder.class.getResource("/res").toURI());
			Process p = Runtime.getRuntime().exec(command/*, null, dir*/);
			p.waitFor();
		} catch (IOException | InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	private static void loadLibs() {
		String osArch = System.getProperty("os.arch");
		if(!Arrays.asList("amd64","x86").contains(osArch)){
        	Alert alert = new Alert(AlertType.ERROR,"Your Operating System is not supported.");
        	alert.showAndWait();
            System.exit(0);
		}
		String normalPath = Main.class.getResource("/"+osArch).getPath();
		String devPath = new File("lib/"+osArch).getAbsolutePath();
		String jarPath = new File("Test_lib/"+osArch).getAbsolutePath();
		System.setProperty("java.library.path", System.getProperty("java.library.path") + 
				File.pathSeparator + normalPath + 
				File.pathSeparator + devPath +
				File.pathSeparator + jarPath);
		try {
			//set sys_paths to null
			final Field sysPathsField = ClassLoader.class.getDeclaredField("sys_paths");
			sysPathsField.setAccessible(true);
			sysPathsField.set(null, null);
			System.out.println("LibaryPath: \"" + System.getProperty("java.library.path"));
			System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
			System.loadLibrary("libmp3lame");
			if(System.getProperty("os.name").contains("Windows")){System.loadLibrary("opencv_ffmpeg320"+(osArch.equals("amd64")?"_64":""));};
		} catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void main(String args[]) throws InterruptedException, IOException {
		//Converter cv = new Converter();
		//cv.run("files/vimu.wav", "files/vimu.mp3");
		System.out.println("Hi");
		String inputVideo = "files/Sequence.mp4";
		String output = new File("files/Sequence_out.mp4").getAbsolutePath();
		String inputAudio = "files/vimu.wav";
		loadLibs();
		int audioSampleRate = 44100;//44.1kHz
		
		/**
		 * Initialize and open required Objects
		 * 
		 * Demuxer: get data out of the Containers
		 * Muxer: combine data in new (Video-)Container
		 * MuxerFormat: Containerformat of the original video
		 * MediaPacket:
		 * MediaAudio[]: contain decoded Audio samples
		 * Codec:
		 * Encoder:
		 */
		final Demuxer audioDemuxer = Demuxer.make();
		final Demuxer videoDemuxer = Demuxer.make();
		final Muxer muxer = Muxer.make(output, null, null);
		final MuxerFormat videoFormat = MuxerFormat.guessFormat(null, inputVideo, null);
	    final MediaPacket videoPacket = MediaPacket.make();
		final Codec audioEncoding = Codec.findEncodingCodecByName("libmp3lame");
		final Encoder audioEncoder = Encoder.make(audioEncoding);

		/**
		 * Recode Audio
		 */
		//Demux & Decode
		audioDemuxer.open(inputAudio, null, true, true, null, null);
		DemuxerStream stream = audioDemuxer.getStream(0);
		Decoder audioDecoder = stream.getDecoder();
		audioDecoder.setChannelLayout(Layout.CH_LAYOUT_MONO);
		audioDecoder.open(null, null);
		logger.log(Level.INFO, "AudioDemuxer& -decoder initialized and opened");
		//Get Input Info
		int channels = audioDecoder.getChannels();
		AudioChannel.Layout tmplay = audioDecoder.getChannelLayout(); // Error Handling Not Channelinfo
		AudioChannel.Layout channelLayout = AudioChannel.Layout.CH_LAYOUT_MONO;//(tmplay==AudioChannel.Layout.CH_LAYOUT_UNKNOWN)?AudioChannel.Layout.CH_LAYOUT_MONO:tmplay;
		AudioFormat.Type iSampleFormat = audioDecoder.getSampleFormat();
		int iSampleRate = audioDecoder.getSampleRate();
		logger.log(Level.INFO, "Audio Stream Information gathered");
		//Set Encoder Parameter
		AudioFormat.Type oSampleFormat = AudioFormat.Type.SAMPLE_FMT_S16P;
		int oSampleRate = 48000;
		//Setup Encoder
		audioEncoder.setSampleFormat(oSampleFormat);
		audioEncoder.setSampleRate(oSampleRate);
		audioEncoder.setChannelLayout(channelLayout);
		audioEncoder.setChannels(channels);
		audioEncoder.setFlag(Coder.Flag.FLAG_GLOBAL_HEADER, true);
		audioEncoder.open(null, null);
		logger.log(Level.INFO, "AudioEncoder opened");
		
		//Setup Resampler
		MediaAudioResampler audioResampler = MediaAudioResampler.make(audioEncoder.getChannelLayout(), audioEncoder.getSampleRate(),
				audioEncoder.getSampleFormat(), audioDecoder.getChannelLayout(), audioDecoder.getSampleRate(), audioDecoder.getSampleFormat());
		audioResampler.open();
		
		/*System.out.println("Output: " + channelLayout + " " + audioEncoder.getSampleRate() + " " +
				audioEncoder.getSampleFormat());
		System.out.println("Input: " + channelLayout + " " + iSampleRate + " " + iSampleFormat);
		*/
		//Setup Videodemuxer
		videoDemuxer.open(inputVideo, null, true, true, null, null);
		int orginalStreams = videoDemuxer.getNumStreams();

		//open all fucking encoder
		List<Decoder> decoderList = new ArrayList<>();
		for (int i = 0; i < orginalStreams; i++) {
			final DemuxerStream ds = videoDemuxer.getStream(i);
			final Decoder d = ds.getDecoder();
			if (d != null) {
				// neat; we can decode. Now let's see if this decoder can fit into the mp4 format.
				if (!videoFormat.getSupportedCodecs().contains(d.getCodecID())) {
					throw new RuntimeException("Input filename (" + inputVideo + ") contains at least one stream with a codec not supported in the output format: " + d.toString());
				}
				if (videoFormat.getFlag(MuxerFormat.Flag.GLOBAL_HEADER))
					d.setFlag(Coder.Flag.FLAG_GLOBAL_HEADER, true);
				decoderList.add(d);
			}
		}
		
		//Recode Audio
		MediaPacket audioInputPacket = MediaPacket.make();
		MediaPacket audioOutputPacket = MediaPacket.make();
		MediaAudio iMediaAudio = MediaAudio.make(22050, iSampleRate, channels, channelLayout, iSampleFormat);
		MediaAudio oMediaAudio = MediaAudio.make(22050, oSampleRate, channels, channelLayout, oSampleFormat);
		muxer.addNewStream(audioEncoder);
		for(Decoder item: decoderList){
			item.open(null, null);
			muxer.addNewStream(item);
		}
		System.out.println("Muxer Streams: " + muxer.getNumStreams());
	    muxer.open(null, null);
	    
		//Iterate over packages

	    while(audioDemuxer.read(audioInputPacket) >= 0) {
			if (audioInputPacket.isComplete()) {
				while(audioDecoder.decodeAudio(iMediaAudio, audioInputPacket, 0) >= 0){
						audioResampler.resample(oMediaAudio, iMediaAudio);
						audioEncoder.encodeAudio(audioOutputPacket, oMediaAudio);
					if(audioOutputPacket.isComplete()){
						muxer.write(audioOutputPacket, false);
						logger.log(Level.INFO, "Write package");
					}
				}
			}
		}
	    while(videoDemuxer.read(videoPacket) >= 0) {
	    	//final Decoder videoDecoder = decoderList.get(videoPacket.getStreamIndex());
	    	if (videoPacket.isComplete()) {
		        /*if (vf != null && videoDecoder.getCodecType() == Type.MEDIA_VIDEO)
			          vf.filter(videoPacket, null);
			        else if (af != null && videoDecoder.getCodecType() == Type.MEDIA_AUDIO)
			          af.filter(videoPacket, null);*/
			        muxer.write(videoPacket, true);
	    	}
	    }
	    muxer.close();
	    videoDemuxer.close();
	    audioDemuxer.close();
	    logger.log(Level.INFO, "Muxer closed");
	    // Create bit stream filters if we are asked to.
	    /*final BitStreamFilter vf = null;
	    final BitStreamFilter af = null;*/
	}
}