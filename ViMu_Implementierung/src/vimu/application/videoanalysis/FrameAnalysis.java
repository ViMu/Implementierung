package vimu.application.videoanalysis;

import java.util.ArrayList;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfFloat;
import org.opencv.core.MatOfInt;
import org.opencv.imgproc.Imgproc; 

/**
 * 
 * @author Paul Bauriegel
 *
 */

public class FrameAnalysis {
	
	public double brightnessMean(Mat frame){
	    // convert frame in HSV Channels
	    List<Mat> images = frameToChannels(frame,true);
		
		// Calculate Histogram
		/* http://docs.opencv.org/2.4/modules/imgproc/doc/histograms.html
		 * List<Mat> images & MatOfInt channels -> only third channel of HSV -> Value = Brightness
		 * Mat mask -> no mask
		 * Mat hist -> write in frame Mat
		 * MatOfInt histSize & MatOfFloat range -> 8-Bit Histogram -> 256 possible Values 
		 * Boolean accumulate -> new histogram -> false */
		Imgproc.calcHist(images.subList(2, 3), new MatOfInt(0), new Mat(), frame, new MatOfInt(256), new MatOfFloat(0, 256), false); //returns array of 256 in frame variable
		//Normalise Histogram
		Core.normalize(frame, frame, 0, frame.rows(), Core.NORM_MINMAX, -1, new Mat());
		
		//return mean of normalised channel
		return Core.mean(frame).val[0];
	}
	
	public Mat[] calcFrameHist(Mat frame,boolean gray, int rows){
		Mat retFrame[] = {new Mat(),new Mat(),new Mat()};
	    List<Mat> images = frameToChannels(frame,false); // convert frame in HSV Channels
	    for(int i = 0; gray? i==0: i<=2; i++){
	    	Imgproc.calcHist(images.subList(i, i+1),new MatOfInt(0), new Mat(), retFrame[i], new MatOfInt(256), new MatOfFloat(0, 256), false);
	    	Core.normalize(retFrame[i], retFrame[i], 0, rows, Core.NORM_MINMAX, -1, new Mat());
	    }
	    return retFrame;
	}
	
	public List<Mat> frameToChannels(Mat frame, boolean toHSV){
	    // convert to HSV colour space
	    if(toHSV) Imgproc.cvtColor(frame, frame, Imgproc.COLOR_BGR2HSV);
	    
	    //split channels
	    List<Mat> images = new ArrayList<>();
		Core.split(frame, images);
		return images;
	}
	
	/* Copy & Paste PyScene Detect */
	public boolean detectContentChange(Mat lastFrame, Mat currentFrame, double threshold){
		if(lastFrame.empty()) return false; //If the Video starts there is no last Frame
		//System.out.println(lastFrame);System.out.println(currentFrame);
		Mat diffFrame = new Mat();
		Core.absdiff(lastFrame, currentFrame, diffFrame); //Difference of the two Frames is written in currentFrame
		double diff = Core.mean(diffFrame).val[0];
		//Calculate the mean difference, TODO only first channel		
		return (diff > threshold);
	}
	
	public boolean detectThresholdChange(Mat lastFrame, Mat currentFrame, double threshold){
		//
		double thresholdLast = Core.sumElems(lastFrame).val[0]/(lastFrame.channels()*lastFrame.cols()*lastFrame.rows());
		double thresholdCurrent = Core.sumElems(currentFrame).val[0]/(lastFrame.channels()*lastFrame.cols()*lastFrame.rows());
		//
		return Math.abs(thresholdLast-thresholdCurrent)>threshold;
	}

}
