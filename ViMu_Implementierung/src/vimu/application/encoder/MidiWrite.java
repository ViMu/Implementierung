package vimu.application.encoder;

/**
 * Class contains methods to convert the datamodel to a midi file
 * This midi file contains one track which is created by a sequencer.
 * Each instrument is assigned to a channel.
 * Each track is created by midi events (message type and tick).
 * 
 * 
 * @author Mai Tran
 * @version 1.0
 * @since   10.05.2017 
 */

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.sound.midi.Instrument;
import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiEvent;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Sequence;
import javax.sound.midi.ShortMessage;
import javax.sound.midi.Soundbank;
import javax.sound.midi.Synthesizer;
import javax.sound.midi.Track;

/**
 * 
 * @author Mai Tran
 *
 */

public class MidiWrite {

	/**
	 * Method that puts everything together
	 * 
	 * @param dataModel
	 */
	public void createMidi(HashMap<String, Object> dataModel) {

		// Track is created by sequencer
		Sequence sequence = null;
		try {
			sequence = new Sequence(javax.sound.midi.Sequence.PPQ, (int) dataModel.get("SpeedOfQuarter"));
		} catch (InvalidMidiDataException e) {
			e.printStackTrace();
		}
		Track song = sequence.createTrack();

		// Extract the elements of dataModel and add them to the track
		int tactStart;
		int channelNo = 0;
		int instrNo;
		int midiKey;
		int velocity;
		int noteStart;
		int noteLength;
		ArrayList<HashMap<String, Object>> scenes = (ArrayList<HashMap<String, Object>>) dataModel.get("Scenes");
		for (HashMap<String, Object> scene : scenes) {
			ArrayList<HashMap<String, Object>> tacts = (ArrayList<HashMap<String, Object>>) scene.get("Takte");
			for (HashMap<String, Object> tact : tacts) {
				tactStart = (int) tact.get("Start");
				HashMap<String, Object> instruments = (HashMap<String, Object>) tact.get("Instrumente");
				for (String instrumentKey : instruments.keySet()) {
					// There are only 16 midi-channels
					if (channelNo > 15) {
						channelNo = 0;
					}
					instrNo = mapToInstrumentKey(instrumentKey); // Map to
																	// manual
																	// list
					// instrNo = mapToInstrSoundbank(instrumentKey); //
					// Alternative: map to name in Soundbank
					song.add(changeInstrument(channelNo, instrNo, tactStart));

					ArrayList<HashMap<String, Object>> instrument = (ArrayList<HashMap<String, Object>>) instruments
							.get(instrumentKey);
					for (HashMap<String, Object> note : instrument) {
						midiKey = mapToMidiKey((int) note.get("Hoehe"), (String) note.get("Ton"));
						velocity = mapToVelocity((int) note.get("Lautstaerke"));
						noteStart = (int) note.get("Start");
						noteLength = (int) note.get("Laenge");

						song.add(noteOnEvent(channelNo, midiKey, velocity, noteStart));
						song.add(noteOffEvent(channelNo, midiKey, velocity, noteStart + noteLength + 1));
					}
					channelNo++;
				}
			}
		}

		// Writes the sequence to MIDI file, second parameter is type (type 0 is
		// one track)
		File vimuMidi = new File(System.getenv("APPDATA") + File.separator + "Vimu" + File.separator + "vimu.mid");
		try {
			MidiSystem.write(sequence, 0, vimuMidi);
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("ViMu-Midifile created");

	}

	/**
	 * Ensures that velocity stays in range
	 * 
	 * @param veloDataModel
	 * @return int
	 */
	public static int mapToVelocity(int veloDataModel) {
		if (veloDataModel > 127) {
			veloDataModel = veloDataModel % 127;
		}
		return veloDataModel;
	}

	/**
	 * Assigns new instrument to a channel
	 * 
	 * @param channel
	 * @param instrNo
	 * @param tic
	 * @return MidiEvent
	 */
	public MidiEvent changeInstrument(int channel, int instrNo, int tic) {
		ShortMessage instrument = new ShortMessage();
		try {
			instrument.setMessage(ShortMessage.PROGRAM_CHANGE, channel, instrNo, 0);
		} catch (InvalidMidiDataException e) {
			e.printStackTrace();
		}
		MidiEvent instrumentEvent = new MidiEvent(instrument, tic);
		return instrumentEvent;
	}

	/**
	 * To play a note it is necessary to turn it on
	 * 
	 * @param channel
	 * @param note
	 * @param velocity
	 * @param tic
	 * @return MidiEvent
	 */
	public MidiEvent noteOnEvent(int channel, int note, int velocity, int tic) {
		ShortMessage noteOn = new ShortMessage();
		try {
			noteOn.setMessage(ShortMessage.NOTE_ON, channel, note, velocity);
		} catch (InvalidMidiDataException e) {

			e.printStackTrace();

		}
		MidiEvent noteOnEvent = new MidiEvent(noteOn, tic);
		return noteOnEvent;
	}

	/**
	 * To end a note it is necessary to turn it off again
	 * 
	 * @param channel
	 * @param note
	 * @param velocity
	 * @param tic
	 * @return MidiEvent
	 */
	public MidiEvent noteOffEvent(int channel, int note, int velocity, int tic) {
		ShortMessage noteOff = new ShortMessage();
		try {
			noteOff.setMessage(ShortMessage.NOTE_OFF, channel, note, velocity);
		} catch (InvalidMidiDataException e) {
			e.printStackTrace();

		}
		MidiEvent noteOffEvent = new MidiEvent(noteOff, tic);
		return noteOffEvent;
	}

	/**
	 * The notes in midi are integers, this method maps the strings to the right integer (default scale is C)
	 * 
	 * @param octave
	 * @param tone
	 * @return int
	 */
	public static int mapToMidiKey(int octave, String tone) {

		String[] majorTones = { "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B" };
		String[] minorTones = { "C", "Db", "D", "Eb", "E", "F", "Gb", "G", "Ab", "A", "Bb", "B" };

		HashMap<String, Integer> toneToInt = new HashMap<>();

		for (int i = 0; i < 12; i++) {
			toneToInt.put(majorTones[i], i);
			toneToInt.put(minorTones[i], i);
		}

		int midiKeyno = 60;
		if (octave > 0 && octave < 11) {
			midiKeyno = toneToInt.get(tone) + 12 * octave;
		}

		return midiKeyno;
	}

	/**
	 * Creates a manual instrumentlist
	 * @param instrument
	 * @return int
	 */
	public static int mapToInstrumentKey(String instrument) {
		HashMap<String, Integer> instrumentList = new HashMap<>();

		instrumentList.put("Piano", 0);
		instrumentList.put("Guitar", 31);

		return instrumentList.get(instrument);
	}

	/**
	 * Alternative method to map the instruments to the default soundbank
	 * @param instrument
	 * @return int
	 */
	public static int mapToInstrSoundbank(String instrument) {
		Synthesizer synth = null;
		try {
			synth = MidiSystem.getSynthesizer();
		} catch (MidiUnavailableException e) {
			e.printStackTrace();
		}
		Soundbank soundbank = synth.getDefaultSoundbank();
		Instrument[] instrumentList = soundbank.getInstruments();

		int instrNo = 0; // default value
		for (int i = 0; i < 128; i++) {
			if (instrument.equals(instrumentList[i].getName())) {
				instrNo = instrumentList[i].getPatch().getProgram();
				return instrNo;
			}
		}

		return instrNo;
	}


}
