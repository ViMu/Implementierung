import pygame

class MidiPlayer:
    def __init__(self, pathToMidi):
        self.path = pathToMidi

    def play(self):
        # pick a midi music file you have ...
        # (if not in working folder use full path)
        music_file = "example.mid"
        freq = 44100  # audio CD quality
        bitsize = -16  # unsigned 16 bit
        channels = 2  # 1 is mono, 2 is stereo
        buffer = 1024  # number of samples
        pygame.mixer.init(freq, bitsize, channels, buffer)
        # optional volume 0 to 1.0
        pygame.mixer.music.set_volume(1.0)
        try:
            self.play_music()
        except KeyboardInterrupt:
            # if user hits Ctrl/C then exit
            # (works only in console mode)
            pygame.mixer.music.fadeout(1000)
            pygame.mixer.music.stop()
            raise SystemExit

    def play_music(self):
        """
        stream music with mixer.music module in blocking manner
        this will stream the sound from disk while playing
        """
        clock = pygame.time.Clock()
        try:
            pygame.mixer.music.load(self.path)
            print "Music file %s loaded!" % self.path
        except pygame.error:
            print "File %s not found! (%s)" % (self.path, pygame.get_error())
            return
        pygame.mixer.music.play()
        while pygame.mixer.music.get_busy():
            # check if playback has finished
            clock.tick(30)
