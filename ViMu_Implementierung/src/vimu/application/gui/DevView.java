package vimu.application.gui;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 * 
 * @author Paul Bauriegel
 *
 */

public class DevView {
	static Logger rootLogger = Logger.getGlobal();
	public void display() {
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/ViMu.Dev.fxml"));
			Parent root = loader.load();
			Stage detailStage = new Stage();

			detailStage.setTitle("ViMu - Developer View");
			// create and style a scene
			Scene scene = new Scene(root);
			scene.getStylesheets().add(getClass().getResource("/fxml/application.css").toExternalForm());
			detailStage.setScene(scene);
			detailStage.show();
		} catch (Exception e) {
			//TODO: Exception Handling
			e.printStackTrace();
		}
	}

}
