package vimu.application.controller;

import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import vimu.application.upload.UploadVideo;

/**
 * 
 * @author Tim Schmidt
 *
 */

public class FxYoutubeController implements Initializable{
	@FXML private javafx.scene.layout.AnchorPane anchorPane;
	@FXML private TextField titelTextField;
	@FXML private TextArea descriptionTextArea;
	@FXML private TextArea tagsTextArea;
	@FXML private ChoiceBox<String> privacyStatusChoiceBox;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		privacyStatusChoiceBox.setItems(FXCollections.observableArrayList("Public","Unlisted", "Private"));
	}

    @FXML protected void handleUploadButton() {
    	if(titelTextField.getText().isEmpty() || descriptionTextArea.getText().isEmpty() || tagsTextArea.getText().isEmpty() || privacyStatusChoiceBox.getSelectionModel().getSelectedItem()==null){

    	Alert alert = new Alert(AlertType.INFORMATION, "Please give all reqiured information."); 
    	 alert.showAndWait();
    	} else {
    	doYourThingLion(titelTextField.getText(),descriptionTextArea.getText(), tagsTextArea.getText(), privacyStatusChoiceBox.getSelectionModel().getSelectedItem().toString());
        Stage mainStage = (Stage)anchorPane.getScene().getWindow();
        mainStage.close();
    	}
    }

	private void doYourThingLion(String title, String description, String tagsString, String privacyStatus) {
		List<String> tags = Arrays.asList(tagsString.split(" "));
		UploadVideo.upload(title,description,tags,privacyStatus);
		
	}
    
    
}