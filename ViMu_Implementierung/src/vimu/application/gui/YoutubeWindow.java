package vimu.application.gui;

import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Screen;
import javafx.stage.Stage;

/**
 * 
 * @author Tim Schmidt
 *
 */

public class YoutubeWindow {
	final int ha = 400; //height of the window after enlargment
	final int w = 600; //width of the window
	
	public YoutubeWindow(){
	}
	
	public void display(){
			try {
				FXMLLoader loader=new FXMLLoader(getClass().getResource("/fxml/youtube.fxml"));
				Parent root = loader.load();
				Stage detailStage = new Stage();
				detailStage.setTitle("Youtube Settings");
				Scene scene = new Scene(root,600,400);
				
				detailStage.setScene(scene);
				
				detailStage.setHeight(ha);
				detailStage.setWidth(w);
				detailStage.getIcons().add(new Image("/images/youtube.png"));
				
				detailStage.show();
				
			} catch(Exception e) {
				
				e.printStackTrace();
			}
	}

	
}
