# Author: Marius Jochheim

# Matschtakte zum Uebergang fehlen

import math

class ModellBuilder:
    def __init__(self, takt, laenge, events, sceneChanges, stil, bpm):
        self.taktart = takt
        self.laenge = laenge
        self.events = events
        self.sceneChanges = sceneChanges
        self.stil = stil
        self.bpm = bpm


    def build(self):
        # Build song corpus
        song = {'Stil': self.stil, 'Taktart': self.taktart, 'Dauer': self.laenge, 'bpm':self.bpm, 'Scenes': []}

        # Build Scenes
        sceneIndex = 0
        for scene in self.sceneChanges:
            song['Scenes'].append({'Start': scene[0], 'Tongeschlecht': scene[1]})
            if sceneIndex != 0:
                song['Scenes'][sceneIndex-1]['Ende'] = scene[0]
            sceneIndex += 1
        song['Scenes'][sceneIndex-1]['Ende'] = self.laenge

        # Build Tact's
        tactLength = math.trunc(self.taktart * 1000.0 * (60.0 / self.bpm))
        song['SpeedOfQuarter'] = math.trunc(tactLength / song['Taktart'])
        for scene in song['Scenes']:
            scene['Takte'] = []
            sceneLength = scene['Ende'] - scene['Start']
            amountTacts = math.trunc(sceneLength / tactLength)
            tempStart = scene['Start']
            for tactNumber in range(0, amountTacts-1):
                scene['Takte'].append({'Taktlaenge': tactLength, 'Start': tempStart, 'Instrumente':{}})
                tempStart += tactLength
                scene['Takte'][tactNumber]['Ende'] = tempStart

        return song

