package vimu.application.composition;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

/**
 * 
 * @author Marius Jochheim
 *
 * Class creates all the necessary information for later generation of notes with accompany instruments.
 * For now only creates a piano accompaniment
 */

public class Begleitung {
    private final double[] lengths = {0.5, 0.25, 0.15, 0.1}; // Probabilities for lengths of notes [eighth, quarter, half, whole]
    private final double[] restValues = {0.0125, 0.25, 0.5, 1.0}; // Rest lengths to iterate over the given possible lengths of the note
	private Random rnd;
		
	Begleitung() {
		rnd = new Random();
	}
	
	/**
	 * 
	 * @param rest Boolean value describing how much time is left in the tact in percent.
	 * @return Returns length of note in percent. Note length is chosen with different probabilities.
	 */
	private double make_length(double rest) {
		double rndValue = rnd.nextDouble();
		
		int i=0;
		for(double d=0; ((d<rndValue || rest == restValues[i])&& i<lengths.length-1); d+=lengths[i],i++)
			;
        return restValues[i];
	}
	
	/**
	 * 
	 * @param dataModel Takes in a reference to the dataModel and automatically appends the information for notes for the accompaniment.
	 */
	public void compose(HashMap<String, Object> dataModel) {	
		Tonleiter scale = new Tonleiter();
		int heightOfTone = 5;
		int lengthOfSong = (int) dataModel.get("Dauer");
		
		ArrayList<HashMap<String, Object>> scenes = ((ArrayList<HashMap<String, Object>>) dataModel.get("Scenes"));
		
		// Iterate over scenes
		for (HashMap<String, Object> scene: scenes) {
			ArrayList<HashMap<String, Object>> tacts = (ArrayList<HashMap<String, Object>>) scene.get("Takte");
			// Iterate over Tacts
			for (HashMap<String, Object> tact: tacts) {
				// Get the root tone of the tact
				ArrayList<String> tones = scale.createTones((int) scene.get("Tongeschlecht"), (String) tact.get("Grundton"));
				int timestamp = (int) tact.get("Start");
				HashMap<String, Object> instruments = (HashMap<String, Object>) tact.get("Instrumente");
				// Gets the list of information for the piano. 
				// TODO: For the enablement of different instruments for accompaniment a loop and different logic has to be developed.
				ArrayList<HashMap<String, Object>> instrument = (ArrayList<HashMap<String, Object>>) instruments.get("Piano");
				double tempLength = 1.0;
				while (tempLength>0) {
					
					// Creating length of note
					double length = make_length(tempLength);
					tempLength -= length;
					length = ((int) tact.get("Taktlaenge"))*length;
					
					// Create Height of Tone
					double rndValue = rnd.nextDouble();
					if (rndValue < 0.05 && heightOfTone > 3) {
						heightOfTone -= 1;
					} else if (rndValue > 0.95 && heightOfTone < 6) {
						heightOfTone += 1;
					}
					
					// Appending notes
					for (int i = 0; i<3; i++) {
						if (rnd.nextDouble() > 0.2) {
							HashMap<String, Object> note = new HashMap<>();
							note.put("Ton", tones.get(i*2));
							
							note.put("Hoehe", heightOfTone);
							if (timestamp + length > lengthOfSong) {
								length = lengthOfSong - timestamp;
							}
							note.put("Laenge", (int) length);
							
							note.put("Lautstaerke", 100);
							note.put("Start", timestamp);
							
							// TODO: For now hinders appending notes that would be longer than the rest of the song. Cleaner Fix needed for length problem.
							if (length > 0) {
								instrument.add(note);
							}
						}
					}					
					timestamp += length;
				}				
			}
		}
	}
}