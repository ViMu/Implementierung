package vimu.application.composition;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

// TODO: Laengen reparieren!
// TODO: 0-Laenge Note am Ende in manchen Faellen.

/**
 * 
 * @author Marius
 *
 * Class creates all the necessary information for generation of notes belonging to the melody.
 */

public class Melodie {
    private Random rnd = new Random();
    private final double[] jumps = {0.2, 0.35, 0.19, 0.01, 0.125, 0.125}; // Probabilities for specific jump [Same Note, Step, Third, Perfect 4th, Perfect 5th, Sixth]
    private final double[] lengths = {0.8, 0.15, 0.02, 0.01, 0.01, 0.01}; // Probabilities for lengths of notes [sixteenth, eighth, quarter, dotted quarter, half, whole]
    private final double[] restValues = {0.0625, 0.0125, 0.25, 0.375, 0.5, 1.0}; // Rest lengths to iterate over the given possible lengths of the note
	
     /**
      * 
      * @return Returns step from one note to another
      */
	private int make_step() {
		double rndValue = rnd.nextDouble();
		
		int i=0;
		for(double d=0; d<rndValue && ++i<jumps.length; d+=jumps[i])
		    ;
        return i;
	}

	 /**
	  * 
	  * @param rest Boolean value describing how much time is left in the tact in percent.
	  * @return Returns length of note in percent. Note length is chosen with different probabilities.
	  */
	private double make_length(double rest) {
		double rndValue = rnd.nextDouble();
		
		int i=0;
		for(double d=0; ((d<rndValue || rest == restValues[i])&& i<lengths.length-1); d+=lengths[i],i++)
			;
        return restValues[i];
	}
	
	/**
	 * 
	 * @param dataModel Needs a reference to the dataModel
	 */
	public void compose(HashMap<String, Object> dataModel) {
		Tonleiter scale = new Tonleiter();
		int heightOfTone = 5;
		int volume = 70;
		int lengthOfSong = (int) dataModel.get("Dauer");
		
		ArrayList<HashMap<String, Object>> scenes = (ArrayList<HashMap<String, Object>>) dataModel.get("Scenes");
		
		for (HashMap<String, Object> scene: scenes) {
			ArrayList<HashMap<String, Object>> tacts = (ArrayList<HashMap<String, Object>>) scene.get("Takte");
			for (HashMap<String, Object> tact: tacts) {
				ArrayList<String> tones = scale.createTones((int) scene.get("Tongeschlecht"), (String) tact.get("Grundton"));
				int timestamp = (int) tact.get("Start");
				HashMap<String, Object> instruments = new HashMap<>();
				tact.put("Instrumente", instruments);
				ArrayList<HashMap<String, Object>> instrument = new ArrayList<>();
				String tone = tones.get(rnd.nextInt(7));
				double tempLength = 1.0;
				while (tempLength>0) {
					// Generating change in tone
					int step = make_step();
					int index = (tones.indexOf(tone) + step) % 7;
					
					// Creating length of note
					double length = make_length(tempLength);
					tempLength -= length;
					length = ((int) tact.get("Taktlaenge"))*length;
					
					// Appending note
					HashMap<String, Object> note = new HashMap<>();
					note.put("Ton", tones.get(index));
					double rndValue = rnd.nextDouble();
					if (rndValue < 0.05 && heightOfTone > 3) {
						heightOfTone -= 1;
					} else if (rndValue > 0.95 && heightOfTone < 6) {
						heightOfTone += 1;
					}
					note.put("Hoehe", heightOfTone);
					if (timestamp + length > lengthOfSong) {
						length = lengthOfSong - timestamp;
					}
					note.put("Laenge", (int) length);
					rndValue = rnd.nextDouble();
					if (rndValue < 0.3 && volume >= 70) {
						volume -= 5;
					} else if (rndValue > 0.8 && volume <= 120) {
						volume += 5;
					}
					note.put("Lautstaerke", volume);
					note.put("Start", timestamp);
					timestamp += length;
					// TODO: For now hinders appending notes that would be longer than the rest of the song. Cleaner Fix needed for length problem.
					if (length > 0) {
						instrument.add(note);
					}
					
					
				}
				instruments.put("Piano", instrument);
			}
		}
	}
}
