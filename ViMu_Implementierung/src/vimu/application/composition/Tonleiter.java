package vimu.application.composition;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

/**
 * 
 * @author Marius Jochheim
 *
 */

public class Tonleiter {
	/**
	 * 
	 * @param tongeschlecht 0 for Major, 1 for Minor
	 * @param root Root tone to create scale
	 * @return returns an ordered list of tones of the scale
	 */
	public ArrayList<String> createTones(int tongeschlecht, String root) {
		// List of generally possible notes
		String[][] notes = {{"C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"},
				{"C", "Db", "D", "Eb", "E", "F", "Gb", "G", "Ab", "A", "Bb", "B"}};
		
		// List of possible scales (0=Dur, 1=Moll)
		int[][] scales = {{2,2,1,2,2,2},{2,1,2,2,1,2}};
		
		// Select list of tones
		String[] selectedNotes = null;
		if (tongeschlecht == 0) {
			if (Arrays.asList("F", "Bb", "Eb", "Ab", "Db", "Gb").contains(root)) {
				selectedNotes = notes[1];
			} else {
				selectedNotes = notes[0];
			}
		} else if (tongeschlecht == 1) {
			if (Arrays.asList("D", "G", "C", "F", "Bb", "Eb", "Db", "Gb", "Ab").contains(root)) {
				selectedNotes = notes[1];
			} else {
				selectedNotes = notes[0];
			}
		}
		
		// Create final scale
		int position = 0;
		for (int i = 0; i<8; i++) {
			if (selectedNotes[i] == root) {
				position = i;
			}
		}
		
		ArrayList<String> returnTones = new ArrayList<String>();
		returnTones.add(root);
		for (int i = 0; i < scales[tongeschlecht].length; i++) {
			position += scales[tongeschlecht][i];
			if (position > selectedNotes.length-1) {
				position = position - selectedNotes.length;
			}
			returnTones.add(selectedNotes[position]);
		}
		
		return returnTones;
	}
	
	/**
	 * 
	 * @param amountScenes
	 * @param listOfMinor
	 * @return
	 */
	public ArrayList<String> getSceneRoots(int amountScenes, ArrayList<Integer> listOfMinor) {
		// TODO Add intelligent transitions
		String[][] notes = {{"C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"},
				{"C", "Db", "D", "Eb", "E", "F", "Gb", "G", "Ab", "A", "Bb", "B"}};
		ArrayList<String> returnList = new ArrayList<String>();
		Random rnd = new Random();
		for (int i = 0; i<amountScenes; i++) {
			returnList.add(notes[rnd.nextInt(2)][rnd.nextInt(12)]);
		}
		
		return returnList;
	}
	
	/**
	 * 
	 * @param tonart
	 * @param tongeschlecht
	 * @param anzahlTakte
	 * @return
	 */
	public ArrayList<String> getTactRoots(String tonart, int tongeschlecht, int anzahlTakte) {
		// TODO Add intelligent transitions
		ArrayList<String> toneList = createTones(tongeschlecht, tonart);
		ArrayList<String> retList = new ArrayList<>();
		Random rnd = new Random();
		for (int i = 0; i<anzahlTakte; i++) {
			retList.add(toneList.get(rnd.nextInt(7)));
		}
		return retList;
	}
	
	/**
	 * 
	 * @param root
	 * @param tongeschlecht
	 * @return
	 */
	public String[] createChord(String root, int tongeschlecht) {
		ArrayList<String> tones = createTones(tongeschlecht, root);
		String[] chord = {tones.get(0), tones.get(2), tones.get(4)};
		return chord;
	}
}
