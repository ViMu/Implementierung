import midi

class Midiwriter:
    def __init__(self, dataModel):
        self.data = dataModel

    def generateMidi(self):
        pathToMidi = "example.mid"

        notesPerInstrument = {}

        for scene in self.data['Scenes']:
            for tact in scene['Takte']:
                for instrument in tact['Instrumente'].keys():
                    if instrument not in notesPerInstrument:
                        notesPerInstrument[instrument] = []
                    for note in tact['Instrumente'][instrument]:
                        notesPerInstrument[instrument].append(note)

        print(notesPerInstrument)

        # Instantiate a MIDI Pattern (contains a list of tracks)
        pattern = midi.Pattern()

        for instrument in notesPerInstrument.keys():
            # Instantiate a MIDI Track (contains a list of MIDI events)
            track = midi.Track()
            # Append the track to the pattern
            pattern.append(track)
            # Create Notes and append to track
            for note in notesPerInstrument[instrument]:
                # Note on Event: tick=Start, pitch=Note Value, velocity=Laustaerke
                toneMapper = {
                    'C':0, 'C#':1, 'Db':1, 'D':2, 'D#':3, 'Eb':3, 'E':4, 'F':5,
                    'F#':6, 'Gb':6, 'G':7, 'G#':8, 'Ab':8, 'A':9, 'A#':10, 'Bb':10,
                    'B':11
                }
                outputTone = toneMapper[note['Ton']] * note['Hoehe']
                midiNoteOn = midi.NoteOnEvent(tick=note['Start'], velocity=note['Lautstaerke'], pitch=outputTone)
                midiNoteOff = midi.NoteOffEvent(tick=note['Start']+note['Laenge'], pitch=outputTone)
                track.append(midiNoteOn)
                track.append(midiNoteOff)
            # Add the end of track event, append it to the track
            eot = midi.EndOfTrackEvent(tick=70000)
            track.append(eot)

        # Print out the pattern
        #print pattern
        # Save the pattern to disk
        midi.write_midifile(pathToMidi, pattern)