package vimu.application.composition;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * 
 * @author Marius Jochheim
 *
 * Controls the control flow in the composition package aswell as defines root tones for every scene and tact.
 */

public class Controller {
	private String style;
	private int bpm;
	private int[][] events;
	private int songlength;
	private String[] instruments;
	private int[][] sceneChanges;
	private int tact;

	/**
	 * 
	 * @param style Defines the style of music to be generated. Not implemented in this version and can take any String.
	 * @param bpm Beats per Minute in the song
	 * @param events List of events where as [int][0] describes the timestamp in milliseconds and [int][1] the type of Event. In this version not implemented.
	 * @param songlength Length of the video in milliseconds.
	 * @param sceneChanges List of scene changes, [int][0] = timestamp in milliseconds, [int][1] = Major(0)/Minor(1)
	 */
	public Controller(String style, int bpm, int[][] events, int songlength, int[][] sceneChanges) {
		this.style=style;
		this.bpm = bpm;
		this.events = events;
		this.songlength =songlength;
		instruments = getInstruments(style);
		this.sceneChanges = sceneChanges;
		tact = getTact();
	}

	private String[] getInstruments(String style) {
		 return new String[]{ "Guitar", "Drums", "Bass", "Piano" };
	}

	private int getTact() {
		return 4;
	}

	/**
	 * 
	 * @param dataModel Needs a reference to the dataModel
	 */
	private void defineSceneScales(HashMap<String, Object> dataModel) {
		// Get a root for every scene
		Tonleiter scale = new Tonleiter();
		ArrayList<HashMap<String, Object>> scenes = (ArrayList<HashMap<String, Object>>) (dataModel.get("Scenes"));
		ArrayList<Integer> minorList = new ArrayList<>();
		for (HashMap<String, Object> scene : scenes) {
			minorList.add((int) scene.get("Tongeschlecht"));
		}
		ArrayList<String> sceneRoots = scale.getSceneRoots(scenes.size(), minorList);

		// Append root to every scene
		int index = 0;
		for (String sceneRoot : sceneRoots) {
			scenes.get(index).put("Tonart", sceneRoot);
			index += 1;
		}
	}

	/**
	 * 
	 * @param dataModel Needs a reference to the dataModel
	 */
	private void defineTactRoots(HashMap<String, Object> dataModel) {
		Tonleiter scale = new Tonleiter();
		ArrayList<HashMap<String, Object>> scenes = (ArrayList<HashMap<String, Object>>) (dataModel.get("Scenes"));
		for (HashMap<String, Object> scene : scenes) {
			ArrayList<HashMap<String, Object>> tacts = (ArrayList<HashMap<String, Object>>) scene.get("Takte");
			ArrayList<String> rootList = scale.getTactRoots(scene.get("Tonart").toString(),
					(int) scene.get("Tongeschlecht"), tacts.size());
			int i = 0;
			for (String root : rootList) {
				((ArrayList<HashMap<String, Object>>) scene.get("Takte")).get(i).put("Grundton", root);
				i += 1;
			}
		}

		return;
	}

	public HashMap<String, Object> compose() {
		// Build initial empty Model
		ModelBuilder modelBuilder = new ModelBuilder(tact, songlength, sceneChanges, style, bpm);
		HashMap<String, Object> dataModel = modelBuilder.build();

		// Define scales for scenes
		defineSceneScales(dataModel);
		
		// Add root tone per tact
		defineTactRoots(dataModel);
		
		// TODO: Analyze event information

		// Add Melody
		Melodie melody = new Melodie();
		melody.compose(dataModel);

		// Add accompaniment
		Begleitung accompanie = new Begleitung();
		accompanie.compose(dataModel);

		// TODO: Change Volume

		// TODO: Smoothen Scene Changes

		// TODO: Event handling 

		return dataModel;
	}

}
