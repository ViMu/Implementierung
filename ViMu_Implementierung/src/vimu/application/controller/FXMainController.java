package vimu.application.controller;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.Slider;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;
import javafx.scene.control.Alert;
import vimu.application.composition.Controller;
import vimu.application.encoder.VideoEncoder;
import vimu.application.encoder.MIDITransmission;
import vimu.application.encoder.MidiWrite;
import vimu.application.gui.YoutubeWindow;
import vimu.application.videoanalysis.EventDetection;
import vimu.application.videoanalysis.VideoImport;
import vimu.application.videoanalysis.VideoImport.UnsupportedVideoFormatException;

/**
 * 
 * @author Tim Schmidt & Paul Bauriegel
 *
 */

public class FXMainController implements Initializable {
	@FXML
	private javafx.scene.layout.AnchorPane anchorPane;
	@FXML
	private ImageView step1;
	@FXML
	private ImageView step2;
	@FXML
	private ImageView step3;
	@FXML
	private ImageView step4;
	@FXML
	private Region dragArea;
	@FXML
	private VBox dragTarget;
	@FXML
	private GridPane gridPane1;
	@FXML
	private StackPane step1pane;
	@FXML
	private StackPane step2pane;
	@FXML
	private StackPane step3pane;
	@FXML
	private StackPane step4pane;
	@FXML
	private ChoiceBox<String> musikStylChoiceBox;
	@FXML
	private Slider backgroundSlider;
	@FXML
	private Label percentLabel;
	@FXML
	private Button vertonenButton;
	@FXML
	private Button direktTeilenButton;
	@FXML
	private ProgressBar loadingProgressBar;
	@FXML
	private javafx.scene.layout.AnchorPane videoAnchor;
	@FXML
	private MediaView resultMediaView;
	@FXML
	private Label videoDisplayName;
	
	// TODO: Beschreibung
	static private Logger logger = Logger.getGlobal();
	private EventDetection eventDetection;
	private String videoPath;
	private String musikStyl;
	private double hintergrundlaut;
	private String sourceVideoName;
	
	private String videoextension;
	private boolean step1active;
	
	String path = "file:///" + ((System.getProperty("os.name").contains("Windows"))?System.getenv("APPDATA").replace('\\', '/'):"/tmp")+"/" + "ViMu"+"/" + "temp.mp4";
	// Create the player and set to play automatically.
	private MediaPlayer mediaPlayer;
	
	public Image getIm(String image){
		String path = null;
		try {
			path = getClass().getResource("/images/"+ image).toURI().toString();
		} catch (URISyntaxException e) {
			logger.log(Level.SEVERE,"File not found: "+"/images/"+ image);
		}
		return new Image(path);
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		step1active = true;
		step1.setImage(getIm("1b.png"));
		musikStylChoiceBox.setItems(FXCollections.observableArrayList("Zufällig", "Rock", "Klassik"));

		backgroundSlider.valueProperty().addListener(new ChangeListener<Number>() {
			public void changed(ObservableValue<? extends Number> arg0, Number arg1, Number arg2) {
				percentLabel.setText("" + new DecimalFormat("#.#").format(arg2.doubleValue()) + "%");
			}
		});

	}

	private void initializeMediaView() {
			System.out.println(path);
			this.mediaPlayer = new MediaPlayer(new Media(path));
			mediaPlayer.setAutoPlay(true);

			// Create the view and add it to the Scene.
			resultMediaView.setMediaPlayer(mediaPlayer);
			resultMediaView.fitWidthProperty().bind(videoAnchor.widthProperty());
			resultMediaView.fitWidthProperty().bind(videoAnchor.widthProperty());
	}

	@FXML
	protected void handleDragTargetKlick() {
		if (step1active) {
			FileChooser chooser = new FileChooser();
			chooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Video", new String[] { "*.mp4", "*.mkv", "*.mov" }));
			chooser.setTitle("Open File");

			try {
				videoPath = chooser.showOpenDialog(anchorPane.getScene().getWindow()).toPath().toString();
			} catch (NullPointerException e) {
				return;
			}

			System.out.println("File was chosen: " + videoPath);
			String[] split = videoPath.split("\\.");
			System.out.println("pathToVideo: " + videoPath);
			videoextension=split[split.length-1];
			System.out.println("extension: " + videoextension);

			String[] split2 = videoPath.split("\\\\");
			sourceVideoName = split2[split2.length-1];
			System.out.println("Name: " + sourceVideoName);

			handleStep2();
		}
	}



	@FXML protected void handleDragOver(DragEvent event){
        Dragboard db = event.getDragboard();
        if (db.hasFiles()) {
        	videoPath = db.getFiles().toString();
        	videoPath=videoPath.substring(1, videoPath.length()-1);
        	System.out.println("File was chosen: " +videoPath);      
		    String[] split=videoPath.split("\\.");
		    videoextension=split[split.length-1];
	    	System.out.println("extension: " + videoextension);

			String[] split2 = videoPath.split("\\\\");
			sourceVideoName = split2[split2.length-1];
			System.out.println("Name: " + sourceVideoName);

        }
        event.consume();
	}

	
	@FXML protected void handleDragDropped(DragEvent event){
        if(!(videoextension.toLowerCase().equals("mp4")||videoextension.toLowerCase().equals("mkv")||videoextension.toLowerCase().equals("mov"))){
			Alert alert = new Alert(AlertType.ERROR, "Unterstützt werden ausschließlich folgende Dateiformate: .mp4 .mkv .mov"); 
			alert.show();
            event.consume();
        } else {
            handleStep2();
            event.consume();
        }
	}
	

	@FXML
	protected void handleStep1() {
		System.out.println("Step1 was klicked.");
		step1pane.setVisible(true);
		step2pane.setVisible(false);
		step3pane.setVisible(false);
		step4pane.setVisible(false);

		step1.setImage(getIm("1b.png"));
		step2.setImage(getIm("2a.png"));
		step3.setImage(getIm("3a.png"));
		step4.setImage(getIm("4a.png"));

		step1active = true;

	}

	@FXML
	protected void handleStep2() {
		System.out.println("Step2 was klicked.");
		videoDisplayName.setText(sourceVideoName);
		
		step1pane.setVisible(false);
		step2pane.setVisible(true);
		step3pane.setVisible(false);
		step4pane.setVisible(false);

		step1.setImage(getIm("1a.png"));
		step2.setImage(getIm("2b.png"));
		step3.setImage(getIm("3a.png"));
		step4.setImage(getIm("4a.png"));

		step1active = false;

	}

	@FXML
	protected void handleStep3() {
		System.out.println("Step3 was klicked.");
		step1pane.setVisible(false);
		step2pane.setVisible(false);
		step3pane.setVisible(true);
		step4pane.setVisible(false);

		step1.setImage(getIm("1a.png"));
		step2.setImage(getIm("2a.png"));
		step3.setImage(getIm("3b.png"));
		step4.setImage(getIm("4a.png"));

		step1active = false;

	}

	@FXML
	protected void handleStep4() {
		System.out.println("Step4 was klicked.");
		step1pane.setVisible(false);
		step2pane.setVisible(false);
		step3pane.setVisible(false);
		step4pane.setVisible(true);

		step1.setImage(getIm("1a.png"));
		step2.setImage(getIm("2a.png"));
		step3.setImage(getIm("3a.png"));
		step4.setImage(getIm("4b.png"));

		step1active = false;
		initializeMediaView();
	}

	protected void handleStep(int number){
		if(number==4) initializeMediaView();
	}
	
	@FXML
	protected void handleVertonenButton() {
		// Daten �bergeben
		try {
			hintergrundlaut = backgroundSlider.getValue();
			musikStyl = musikStylChoiceBox.getSelectionModel().getSelectedItem().toString();
		} catch (java.lang.NullPointerException e) {
			// TODO: Expetion Handling
			logger.log(Level.SEVERE, "No input!");
		}
		logger.log(Level.INFO, "Pfad zum Video: " + videoPath + "\n Hintergrundlaut: " + hintergrundlaut + "\n musikStyl: " + musikStyl);

		// start Video analysis ( pathToVideo )
		new Thread() {
			@Override
			public void run() {
				handleStep3();
				final String path = System.getenv("APPDATA")+File.separator+ "ViMu"+File.separator;
				VideoImport videoFrames;
				try {
					videoFrames = new VideoImport(videoPath);
					eventDetection = new EventDetection(videoFrames);
					System.out.println(eventDetection.detectScenes());
				} catch (UnsupportedVideoFormatException e) {
					// TODO ErrorHandling Videoimport
					this.interrupt();
					e.printStackTrace();
					return;
				}
				loadingProgressBar.setProgress(0.10);
				eventDetection = new EventDetection(videoFrames);
				//System.out.println(eventDetection.detectScenes());
				loadingProgressBar.setProgress(0.40);
				// Array of Events/Scenes
				int[][] scenes = { { 0, 1 }, { 25000, 0 }, { 45000, 1 } };
				scenes = eventDetection.toTimestamp(30,eventDetection.detectScenes());
				Controller composer = new Controller("Rock", 120, new int[][] { {} }, 60000, scenes);
				HashMap<String, Object> myDataset = composer.compose();
				loadingProgressBar.setProgress(0.70);
				MidiWrite midifile = new MidiWrite();
				midifile.createMidi(myDataset);
				loadingProgressBar.setProgress(0.80);
				MIDITransmission.convertMidiFile(path + "vimu.mid", path + "vimu.wav");
				// Encoder.encode(".mp4", videoPath);
				loadingProgressBar.setProgress(0.95);
				VideoEncoder.cmdWrapper(videoPath, path + "vimu.wav", path + "temp.mp4", hintergrundlaut);
				loadingProgressBar.setProgress(1);
				handleStep4(); // auf Ladeseite wechseln
			}
		}.start();
	}

	@FXML
	protected void handleHochladenButton() {
		// Daten übergeben

		YoutubeWindow ytw = new YoutubeWindow();
		ytw.display();

	}

	@FXML
	protected void handleVideoSpeichernButton() {
		FileChooser chooser = new FileChooser();
		chooser.setTitle("Save Video");
		chooser.setInitialDirectory(new File("C:/"));
		chooser.setInitialFileName("ViMu." + videoextension);
		chooser.setSelectedExtensionFilter(new ExtensionFilter("Videoformat", videoextension));
		try {
			chooser.showSaveDialog(anchorPane.getScene().getWindow()).toPath().toString();
		} catch (NullPointerException e) {
			return;
		}
		System.out.println("Save video here: " + videoPath);
		// TO DO: actually save the video
	}

	@FXML
	protected void handleErneutVertonenButton() {
		// Daten �bergeben
		
		hintergrundlaut = 0;
		musikStyl = null;
		videoPath = null;
		handleStep1();
		System.out.println("Reset.");
		handleStep1(); // back to first page
		mediaPlayer.pause();
	}

	@FXML
	protected void handleDirektTeilenButton() {
		path="file:///"+videoPath.replace("\\", "/");
		handleStep4();
	}

	@FXML
	protected void handleBeendenButton() {
		Stage mainStage = (Stage) anchorPane.getScene().getWindow();
		mainStage.close();
	}

}