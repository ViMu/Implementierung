package vimu.application.composition;

import java.util.ArrayList;
import java.util.HashMap;

import vimu.application.encoder.MIDITransmission;
import vimu.application.encoder.MidiWrite;

/**
 * 
 * @author Marius Jochheim
 * 
 * Testing class
 *
 */

public class Main {
	public static void main(String[] args) {
		// Array of Events
		int[][] events = { { 25000, 90 }, { 45000, 88 } };
		// Array of Scenes
		int[][] scenes2 = { { 0, 1 }, { 25000, 0 }, { 45000, 1 } };

		Controller composer = new Controller("Rock", 60, events, 60000, scenes2);
		HashMap<String, Object> dataModel = composer.compose();
		
		System.out.println(dataModel.toString());
		
		ArrayList<HashMap<String, Object>> scenes = ((ArrayList<HashMap<String, Object>>) dataModel.get("Scenes"));
		
		for (HashMap<String, Object> scene: scenes) {
			ArrayList<HashMap<String, Object>> tacts = (ArrayList<HashMap<String, Object>>) scene.get("Takte");
			for (HashMap<String, Object> tact: tacts) {
				HashMap<String, Object> instruments = (HashMap<String, Object>) tact.get("Instrumente");
				ArrayList<HashMap<String, Object>> instrument = (ArrayList<HashMap<String, Object>>) instruments.get("Piano");
				
				System.out.println(instrument);
			}
		}
		
		MidiWrite midiwriter = new MidiWrite();
		midiwriter.createMidi(dataModel);
		MIDITransmission.convertMidiFile("files/vimu.mid", "files/vimu2.wav");
	}
}
