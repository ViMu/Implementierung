package vimu.application.controller;

import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.opencv.core.*;
import org.opencv.imgproc.Imgproc;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.Slider;
import javafx.scene.control.SplitPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import vimu.application.videoanalysis.EventDetection;
import vimu.application.videoanalysis.FrameAnalysis;
import vimu.application.videoanalysis.VideoImport;
import vimu.application.videoanalysis.VideoImport.UnsupportedVideoFormatException;

/**
 * 
 * @author Paul Bauriegel
 * 
 */

public class FXDevController {
	static private Logger logger = Logger.getGlobal();
	
	private ScheduledExecutorService timer = Executors.newSingleThreadScheduledExecutor();
	private FrameAnalysis fa = new FrameAnalysis();
	private EventDetection eventDetection;
	private String videoPath = "files/Sequence.mp4";

	// <editor-fold defaultstate="collapsed" desc="@FXML Declarations">
	@FXML private Button start;
	@FXML private ImageView videoplay;
	@FXML private ImageView histogram;
	@FXML private ImageView maskImage;
	@FXML private ImageView morphImage;
	@FXML private SplitPane splitPane;
	@FXML private Slider hueStart;
	@FXML private Slider hueStop;
	@FXML private Slider saturationStart;
	@FXML private Slider saturationStop;
	@FXML private Slider valueStart;
	@FXML private Slider valueStop;
	@FXML private ProgressIndicator progressStatus;
	@FXML private AnchorPane histPane;
	// </editor-fold>

	private ObjectProperty<String> hsvValuesProp = new SimpleObjectProperty<>();

	@FXML
	protected void loadVideo(ActionEvent event) throws UnsupportedVideoFormatException {
		// Import Video
		// loads the items at another thread, asynchronously
		// grab a frame every 33 ms (30 frames/sec)
		Runnable frameGrabber = new Runnable() {
			VideoImport videoFrames = new VideoImport(videoPath);
			@Override
			public void run() {
				try {
					// Grab, Convert and Display one frame
					Mat currentFrame = videoFrames.next();
					showImage(videoplay, currentFrame);
					//showHistogram(currentFrame, false);
					//showHistogramold(currentFrame, false);
					// redrawFrame(currentFrame);
					// fa.brightnessMean(currentFrame);
				} catch (Exception e) {
					e.printStackTrace();
					try {
						videoFrames.close();
					} catch (Exception e1) {
						e1.printStackTrace();
					}
				}
			}
		};
		new Thread() {
			@Override
			public void run() {
				try (VideoImport videoFrames = new VideoImport(videoPath);){
					
					eventDetection = new EventDetection(videoFrames);
					System.out.println(eventDetection.detectScenes());
				} catch (Exception e) {
					// TODO ErrorHandling Videoimport
					e.printStackTrace();
				}
				timer.scheduleAtFixedRate(frameGrabber, 0, 33, TimeUnit.MILLISECONDS);
			}
		}.start();
	}

	public void showImage(ImageView view, Mat frame) {
		Image imageToShow = Utils.mat2Image(frame);
		double scale = (splitPane.getWidth() / 2) / imageToShow.getWidth();// scale to half of parent Pane
		int width = (int) Math.round(scale * imageToShow.getWidth());
		int height = (int) Math.round(scale * imageToShow.getHeight());
		imageToShow = createResizedCopy(imageToShow, width, height, false);
		Utils.onFXThread(view.imageProperty(), imageToShow);
	}

	Image createResizedCopy(Image originalImage, int scaledWidth, int scaledHeight, boolean preserveAlpha) {
		int imageType = preserveAlpha ? BufferedImage.TYPE_INT_RGB : BufferedImage.TYPE_INT_ARGB;
		BufferedImage bufferImage = new BufferedImage((int) originalImage.getWidth(), (int) originalImage.getHeight(),
				imageType);
		SwingFXUtils.fromFXImage(originalImage, bufferImage);
		BufferedImage scaledBI = new BufferedImage(scaledWidth, scaledHeight, imageType);
		Graphics2D g = scaledBI.createGraphics();
		if (preserveAlpha) {
			g.setComposite(AlphaComposite.Src);
		}
		g.drawImage(bufferImage, 0, 0, scaledWidth, scaledHeight, null);
		g.dispose();
		return SwingFXUtils.toFXImage(scaledBI, null);
	}

	public void showHistogram(Mat frame, boolean gray) {
		// draw the histogram
		int histRange = 256;
		Mat calcHist[] = fa.calcFrameHist(frame, gray, histRange);
		Mat drawHist = new Mat(histRange, histRange, CvType.CV_8UC3);
		double binWidth = histPane.getWidth() / (double) histRange;
		double binHeight = histPane.getHeight() / (double) histRange;
		//Imgproc.line(drawHist,new Point(0,0),new Point(256,256),new Scalar(255, 0, 0), 2, 8, 0);
		for (int i = 0; i < histRange - 1; i++) {// von links nach rechts
			for(int j = 0; j <= 2; j++) {
				Imgproc.line(drawHist, new Point(binWidth * (i), histPane.getHeight() - binHeight * calcHist[j].get(i, 0)[0]),
						new Point(binWidth * (i + 1), histPane.getHeight() - binHeight * calcHist[j].get(i + 1, 0)[0]), // get[0], weil einspaltig
						new Scalar(j==0?255:0, j==1?255:0, j==2?255:0), 2, 8, 0);
						//new Scalar(255, 0, 0), 2, 8, 0);
			}
		}
		Image histImg = Utils.mat2Image(drawHist);
		Utils.onFXThread(this.histogram.imageProperty(), histImg);

	}
	
	private void showHistogramold(Mat frame, boolean gray) {
		// split the frames in multiple images
		//Imgproc.cvtColor(frame, frame, Imgproc.COLOR_BGR2HSV);
		List<Mat> images = new ArrayList<Mat>();
		Core.split(frame, images);

		// set the number of bins at 256
		MatOfInt histSize = new MatOfInt(256);
		// only one channel
		MatOfInt channels = new MatOfInt(0);
		// set the ranges
		MatOfFloat histRange = new MatOfFloat(0, 256);
		List<Mat> images1 = fa.frameToChannels(frame,false);

		// compute the histograms for the B, G and R components
		Mat hist_b = new Mat();
		Mat hist_g = new Mat();
		Mat hist_r = new Mat();
		Mat hist_d = fa.calcFrameHist(frame, true, 256)[0];

		// B component or gray image
		Imgproc.calcHist(images.subList(0, 1), channels, new Mat(), hist_b, histSize, histRange, false);
		//System.out.println("Source:"+ images.get(0));
		//System.out.println("Hist: " + hist_b.dump());
		// G and R components (if the image is not in gray scale)
		if (!gray)
		{
			Imgproc.calcHist(images.subList(1, 2), channels, new Mat(), hist_g, histSize, histRange, false);
			Imgproc.calcHist(images.subList(2, 3), channels, new Mat(), hist_r, histSize, histRange, false);
		}

		// draw the histogram
		int hist_w = 150; // width of the histogram image
		int hist_h = 150; // height of the histogram image
		int bin_w = (int) Math.round(hist_w / histSize.get(0, 0)[0]);

		Mat histImage = new Mat(hist_h, hist_w, CvType.CV_8UC3, new Scalar(0, 0, 0));
		// normalize the result to [0, histImage.rows()]
		Core.normalize(hist_b, hist_b, 0, histImage.rows(), Core.NORM_MINMAX, -1, new Mat());

		// for G and R components
		if (!gray)
		{
			Core.normalize(hist_g, hist_g, 0, histImage.rows(), Core.NORM_MINMAX, -1, new Mat());
			Core.normalize(hist_r, hist_r, 0, histImage.rows(), Core.NORM_MINMAX, -1, new Mat());
		}

		// effectively draw the histogram(s)
		for (int i = 1; i < histSize.get(0, 0)[0]; i++)
		{
			// B component or gray image
			Imgproc.line(histImage, new Point(bin_w * (i - 1), hist_h - Math.round(hist_b.get(i - 1, 0)[0])),
					new Point(bin_w * (i), hist_h - Math.round(hist_b.get(i, 0)[0])), new Scalar(255, 0, 0), 2, 8, 0);
			// G and R components (if the image is not in gray scale)
			if (!gray)
			{
				Imgproc.line(histImage, new Point(bin_w * (i - 1), hist_h - Math.round(hist_g.get(i - 1, 0)[0])),
						new Point(bin_w * (i), hist_h - Math.round(hist_g.get(i, 0)[0])), new Scalar(0, 255, 0), 2, 8,
						0);
				Imgproc.line(histImage, new Point(bin_w * (i - 1), hist_h - Math.round(hist_r.get(i - 1, 0)[0])),
						new Point(bin_w * (i), hist_h - Math.round(hist_r.get(i, 0)[0])), new Scalar(0, 0, 255), 2, 8,
						0);
			}
		}

		// display the histogram...
		Image histImg = Utils.mat2Image(histImage);
		Utils.onFXThread(this.histogram.imageProperty(), histImg);

	}

	private void redrawFrame(Mat frame) {
		// init
		Mat blurredImage = new Mat();
		Mat hsvImage = new Mat();
		Mat mask = new Mat();
		Mat morphOutput = new Mat();

		// remove some noise
		Imgproc.blur(frame, blurredImage, new Size(7, 7));

		// convert the frame to HSV
		Imgproc.cvtColor(blurredImage, hsvImage, Imgproc.COLOR_BGR2HSV);

		// get thresholding values from the UI
		// remember: H ranges 0-180, S and V range 0-255
		Scalar minValues = new Scalar(this.hueStart.getValue(), this.saturationStart.getValue(),
				this.valueStart.getValue());
		Scalar maxValues = new Scalar(this.hueStop.getValue(), this.saturationStop.getValue(),
				this.valueStop.getValue());

		// show the current selected HSV range
		String valuesToPrint = "Hue range: " + minValues.val[0] + "-" + maxValues.val[0] + "\tSaturation range: "
				+ minValues.val[1] + "-" + maxValues.val[1] + "\tValue range: " + minValues.val[2] + "-"
				+ maxValues.val[2];
		Utils.onFXThread(this.hsvValuesProp, valuesToPrint);

		// threshold HSV image to select tennis balls
		Core.inRange(hsvImage, minValues, maxValues, mask);
		// show the partial output
		showImage(this.maskImage, mask);

		// morphological operators
		// dilate with large element, erode with small ones
		Mat dilateElement = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(24, 24));
		Mat erodeElement = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(12, 12));

		Imgproc.erode(mask, morphOutput, erodeElement);
		Imgproc.erode(morphOutput, morphOutput, erodeElement);

		Imgproc.dilate(morphOutput, morphOutput, dilateElement);
		Imgproc.dilate(morphOutput, morphOutput, dilateElement);

		// show the partial output
		showImage(this.morphImage, morphOutput);

		// find the tennis ball(s) contours and show them
		findAndDrawBalls(morphOutput, frame);
	}

	private void findAndDrawBalls(Mat maskedImage, Mat frame) {
		// init
		List<MatOfPoint> contours = new ArrayList<>();
		Mat hierarchy = new Mat();

		// find contours
		Imgproc.findContours(maskedImage, contours, hierarchy, Imgproc.RETR_CCOMP, Imgproc.CHAIN_APPROX_SIMPLE);

		// if any contour exist...
		if (hierarchy.size().height > 0 && hierarchy.size().width > 0) {
			// for each contour, display it in blue
			for (int idx = 0; idx >= 0; idx = (int) hierarchy.get(0, idx)[0]) {
				Imgproc.drawContours(frame, contours, idx, new Scalar(250, 0, 0));
			}
		}
	}
}