package vimu.application.videoanalysis;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.opencv.core.Mat;

/**
 * 
 * @author Paul Bauriegel
 *
 */

@SuppressWarnings("unused")
public class EventDetection {

	static final Logger logger = Logger.getGlobal();
	Random random =new Random();
	private FrameAnalysis frameAnalysis = new FrameAnalysis();
	private VideoImport videoFrames;

	public EventDetection(VideoImport videoFrames) {
		this.videoFrames = videoFrames;
	}
	
	public List<Integer> detectScenes() {
		Mat lastFrame = new Mat();
		int frameCount = 0;
		List<Integer> scenes = new ArrayList<>();

		// Iterate over frames to detectCut
		while (videoFrames.hasNext()) {
			Mat currentFrame = videoFrames.next();
			currentFrame = frameAnalysis.frameToChannels(currentFrame, true).get(0);
			boolean contentChangeDetected = frameAnalysis.detectContentChange(lastFrame, currentFrame, 30);
			boolean thresholdChangeDetected = frameAnalysis.detectThresholdChange(lastFrame, currentFrame, 30);
			if (contentChangeDetected || thresholdChangeDetected) {
				scenes.add(frameCount);
			}
			lastFrame = currentFrame;
			frameCount++;
		}
		scenes.add(frameCount);
		return scenes;
	}

	public List<Integer> detectMotion() {
		// TODO detect Motion in Video
		return null;
	}
	
	public int[][] toTimestamp(double fps,List<Integer> list){
		int i = 0;
		int[][] newList = new int[list.size()][2];
		for(Integer entry: list){
			newList[i][0] = (int)((entry/fps)*1000);
			newList[i][1] = (int)(random.nextInt(1));
			i++;
		}
		return newList;
	}
}
