# Author: Marius Jochheim
import pprint

from Controller import Composer
from Midiwriter import Midiwriter
from MidiPlayer import MidiPlayer

# Events: Typ, ID, timestamp, confidence (Typ 0 gleich Szenenwechsel)
events = [[25000, 90], [45000, 88]]
# Liste von Szenenwechsel, Start in Millisekunden, Tongeschlecht (0=Dur, 1=Moll)
scenes = [[0, 1],[25000, 0],[45000, 1]]
information = {'style': 'Rock', 'bpm': 60, 'events': events, 'songlength': 60000, 'sceneChanges': scenes}

myComposer = Composer(information)
myDataset = myComposer.compose()

pp = pprint.PrettyPrinter()
print('Result:')
#print pp.pprint(myDataset)

#print 'Generating Midi'
myMidi = Midiwriter(dataModel=myDataset)
myMidi.generateMidi()

#print 'Playing Sound'
#myMidiPlayer = MidiPlayer('example.mid')
#myMidiPlayer.play()