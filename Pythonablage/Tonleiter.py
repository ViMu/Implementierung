from random import choice

class Tonleiter:
    def createTones(self, scale, root):
        # Dic of generally possible Notes
        notes = {
            'sharps': ['C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#', 'A', 'A#', 'B'],
            'flats': ['C', 'Db', 'D', 'Eb', 'E', 'F', 'Gb', 'G', 'Ab', 'A', 'Bb', 'B']
        }

        # Dic of possible scales (0=Dur, 1=Moll)
        scales = {
            0: [2,2,1,2,2,2],
            1: [2,1,2,2,1,2] # Eventuell durch harmonische Moll ersetzen
        }

        # Select list of tones
        if (scale == 0): #Dur
            if root in ['F', 'Bb', 'Eb', 'Ab', 'Db', 'Gb']: #'F', 'Bb', 'Eb', 'Ab', 'Db', 'Gb'
                selectedNotes = notes['flats']
            else:
                selectedNotes = notes['sharps']
        elif (scale == 1): #Moll
            if root in ['D', 'G', 'C', 'F', 'Bb', 'Eb', 'Db', 'Gb', 'Ab']: #'D', 'G', 'C', 'F', 'Bb', 'Eb'
                selectedNotes = notes['flats']
            else:
                selectedNotes = notes['sharps']

        # Create final scale
        position = selectedNotes.index(root)
        returnTones = [root]
        for i in range(len(scales[scale])):
            position += scales[scale][i]
            if (position > len(selectedNotes)-1):
                position = position - len(selectedNotes)
            returnTones.append(selectedNotes[position])
        return returnTones

    def getSceneRoots(self, amountScenes, listOfMinor):
        # ToDo Add intelligent transitions
        notes = {
            'sharps': ['C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#', 'A', 'A#', 'B'],
            'flats': ['C', 'Db', 'D', 'Eb', 'E', 'F', 'Gb', 'G', 'Ab', 'A', 'Bb', 'B']
        }
        returnList = []
        for i in range(amountScenes):
            returnList.append([choice(notes[choice(notes.keys())]), listOfMinor[i]])
        return returnList

    def getTactRoots(self, tonart, tongeschlecht, anzahlTakte):
        # ToDo Add intelligent transitions
        toneList = self.createTones(tongeschlecht, tonart)
        retList = []
        for i in range(anzahlTakte):
            retList.append(choice(toneList))

        return retList

    def createChord(self, root, tongeschlecht):
        tones = self.createTones(tongeschlecht, root)
        chord = []
        chord.append(tones[0])
        chord.append(tones[2])
        chord.append(tones[4])
        return chord

    def testScales(self):
        notes = {
            'sharps': ['C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#', 'A', 'A#', 'B'],
            'flats': ['C', 'Db', 'D', 'Eb', 'E', 'F', 'Gb', 'G', 'Ab', 'A', 'Bb', 'B']
        }

        tongeschlecht = [0,1]
        scale = Tonleiter()

        for i in tongeschlecht:
            print('Tongeschlecht: ', i)
            for a in notes.keys():
                print(a, ':')
                for note in notes[a]:
                    print(scale.createTones(scale=i, root=note))

