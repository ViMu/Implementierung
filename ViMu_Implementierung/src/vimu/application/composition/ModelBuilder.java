package vimu.application.composition;

import java.util.ArrayList;
import java.util.HashMap;

// TODO Shortened tacts for scene changes missing

/**
 * 
 * @author Marius Jochheim
 *
 */

public class ModelBuilder {
	private int tact;
	private int length;
	private int[][] sceneChanges;
	private String style;
	private int bpm;
	
	ModelBuilder(int pTact, int pLength, int[][] pSceneChanges, String pStyle, int pbpm) {
		tact = pTact;
		length = pLength;
		sceneChanges = pSceneChanges;
		style = pStyle;
		bpm = pbpm;		
	}

	public HashMap<String, Object> build() {
		// Build song corpus
		HashMap<String, Object> song = new HashMap<String, Object>();
		song.put("Stil", style);
		song.put("Taktart", tact);
		song.put("Dauer", length);
		song.put("bpm", bpm);
		
		// Empty List of Hashmaps for appending Scenes
		ArrayList<HashMap<String, Object>> scenes = new ArrayList<HashMap<String, Object>>();
		
		// Build Scenes
		int sceneIndex = 0;
		for (int[] scene: sceneChanges) {
			HashMap<String, Object> sceneMap = new HashMap<String, Object>();
			sceneMap.put("Start", scene[0]);
			sceneMap.put("Tongeschlecht", scene[1]);
			if (sceneIndex < sceneChanges.length-1) {
				sceneMap.put("Ende", sceneChanges[sceneIndex+1][0]);
			} else if (sceneIndex == sceneChanges.length-1) {
				sceneMap.put("Ende", length);
			}
			scenes.add(sceneIndex, sceneMap);
			sceneIndex += 1;
		}
		song.put("Scenes", scenes);
		
		// Build Tacts
		int tactLength = (int) (Math.floor((float)(tact) * 1000.0 * (60.0 / (float)(bpm))));
		int speedOfQuarter = tactLength / tact;
		song.put("SpeedOfQuarter", speedOfQuarter);
		for (HashMap<String, Object> scene: scenes) {
			ArrayList<HashMap<String, Object>> tacts = new ArrayList<>();
			int sceneLength = (int) (scene.get("Ende")) - (int) (scene.get("Start"));
			int amountTacts = sceneLength / tactLength;
			int tempStart = (int) scene.get("Start");
			for (int tactNumber = 0; tactNumber <= amountTacts; tactNumber++) {
				HashMap<String, Object> tempTact = new HashMap<>();
				tempTact.put("Taktlaenge", tactLength);
				tempTact.put("Start", tempStart);
				HashMap<String, Object> instruments = new HashMap<>();
				tempTact.put("Instrumente", instruments);
				tempStart += tactLength;
				tempTact.put("Ende", tempStart);
				tacts.add(tempTact);
			}
			
			scene.put("Takte", tacts);
		}
		
		return song;
	}
}
