package vimu.application.encoder;

import jm.util.Write;
import jm.util.*;

/**
 * 
 * @author Paul Bauriegel
 *
 */

public class MIDITransmission {
	public static void convertMidiFile(String inputPath, String outputPath){
		float[] data = Read.audio(inputPath);
		Write.audio(data, outputPath);
	}
}
