
package vimu.application.videoanalysis;

import java.io.IOException;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.opencv.core.Mat;
import org.opencv.videoio.VideoCapture;
import org.opencv.videoio.Videoio;

/**
 * 
 * @author Paul Bauriegel
 *
 */

public class VideoImport implements Iterator<Mat>, AutoCloseable {

	private final static Logger LOGGER = Logger.getGlobal();

	private VideoCapture videoCapture;

	private Mat nextFrame;

	public VideoImport(String videoPath) throws UnsupportedVideoFormatException {
		videoCapture = new VideoCapture(videoPath);
		if (!videoCapture.isOpened()) {// Check if Video opened
			LOGGER.log(Level.SEVERE, "VideoCapture cannot be opened. Maybe the Codec is not supported.");
			throw new UnsupportedVideoFormatException(videoPath);
		}
		LOGGER.log(Level.INFO, "Height: " + getHeigth() + " Width: " + getWidth() + " FPS: " + getFPS());
		readNext();
	}

	private double getWidth() {
		return videoCapture.get(Videoio.CAP_PROP_FRAME_WIDTH);
	}

	private double getHeigth() {
		return videoCapture.get(Videoio.CAP_PROP_FRAME_HEIGHT);
	}

	private double getFPS() {
		return videoCapture.get(Videoio.CAP_PROP_FPS);
	}

	public static class UnsupportedVideoFormatException extends IOException {
		public UnsupportedVideoFormatException(String videoPath) {
			super("Cant open: " + videoPath);
		}

		private static final long serialVersionUID = 6805146603821075715L;
	}

	@Override
	public boolean hasNext() {
		return nextFrame != null;
	}

	private void readNext() {
		Mat nextFrame = new Mat();
		if (!videoCapture.read(nextFrame)){
			nextFrame = null;
			videoCapture.release();
		}
	}

	@Override
	public Mat next() {
		if (nextFrame == null)
			throw new NoSuchElementException();
		Mat ret = nextFrame;
		readNext();
		return ret;
	}

	@Override
	public void close() throws Exception {
		videoCapture.release();
	}
}